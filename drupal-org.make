; REQUIRED ATTRIBUTES
core = 6.16

; OPTIONAL ATTRIBUTES

; THEMES
projects[fusion] = 1.0-rc1
projects[acquia_prosper] = 1.0-beta4

; MODULES - basic
projects[token] = 1.12
projects[cck] = 2.6
projects[link] = 2.8
projects[imageapi] = 1.6
projects[filefield] = 3.2

; MODULES - content display
projects[imagecache] = 2.0-beta10
projects[imagefield] = 3.2
projects[insert] = 1.0-beta4
projects[print] = 1.10
projects[skinr] = 1.5
projects[stringoverrides] = 1.7
projects[submitted_by] = 1.4
projects[term_display] = 1.1
projects[views] = 2.10

; MODULES - navigation
projects[accountmenu] = 1.6
projects[blocks404] = 1.2
projects[special_menu_items] = 1.5
projects[menu_breadcrumb] = 1.3
projects[menu_block] = 2.3

; MODULES - administration
projects[adminrole] = 1.2
projects[admin_menu] = 1.5
projects[backup_migrate] = 2.2
projects[better_formats] = 1.2
projects[css_injector] = 1.3
projects[menu_editor] = 2.0-beta2
projects[mollom] = 1.13
projects[submitagain] = 1.2
projects[vertical_tabs] = 1.0-rc1

; MODULES - SEO
projects[page_title] = 2.3
projects[nodewords] = 1.11
projects[pathauto] = 1.3

; TBA
;projects[themesettings] =
;projects[backup_migrate_files] =
